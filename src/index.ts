import plugin from 'tailwindcss/plugin';

// Utilities
import { COLORS } from './utilities/colors.utility';
import { TYPOGRAPHY } from './utilities/typography.utility';
import { BORDER_RADIUS } from './utilities/borderRadius.utility';
import { SHADOW } from './utilities/boxShadow.utility';
import { BACKGROUND } from './utilities/background.utility';
import { TRANSITION } from './utilities/transitions.utility';
import { KEYFRAMES, ANIMATIONS } from './utilities/animations.utility';

export const andesPlugin = {
  corePlugins: {
    fontSize: false,
  },
  theme: {
    fontFamily: {
      sans:
        'Inter,-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen,Ubuntu,Cantarell,"Open Sans","Helvetica Neue",sans-serif',
    },
    colors: COLORS,
    borderRadius: BORDER_RADIUS,
    boxShadow: SHADOW,
    extend: {
      flex: {
        '1-0': '1 0 auto',
      },
      keyframes: KEYFRAMES,
      animation: ANIMATIONS,
    },
  },

  plugins: [
    plugin(function ({ addUtilities }) {
      addUtilities(TYPOGRAPHY);
      addUtilities(TRANSITION);
      addUtilities(BACKGROUND);
    }),
  ],
};

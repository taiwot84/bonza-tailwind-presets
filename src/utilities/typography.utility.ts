import { FONT_DEFINITIONS } from '../@types/tailwind.interface';

function generateTypography() {
  const fontStyles: FONT_DEFINITIONS[] = [
    ['display1', 52, 64, 400, '0.2px'],
    ['display2', 36, 48, 400, '0.2px'],
    ['h1', 36, 44, 500, 0],
    ['h2', 28, 36, 500, 0],
    ['h3', 18, 26, 500, 0],
    ['h4', 16, 22, 700, 0],
    ['h5', 12, 24, 700, 0],
    ['label1', 18, 26, 600, 0.2],
    ['label2', 16, 24, 700, 0.1],
    ['label3', 14, 24, 700, 0.1],
    ['label4', 12, 20, 700, 0.1],
    ['label5', 10, 14, 700, 0.05],
    ['body-xl', 18, 32, 500, 0],
    ['body-l', 16, 28, 500, 0],
    ['body-m', 14, 26, 500, 0],
    ['body-s', 12, 22, 500, 0],
    ['body-xs', 10, 16, 500, 0],
    ['data-label', 9, 12, 500, 0.06],
  ];

  const classes = {};

  fontStyles.forEach((fontStyle) => {
    const [name, size, lineHeight, fontWeight, ltrSpacing] = fontStyle;

    const letterSpacing =
      typeof ltrSpacing === 'string' ? ltrSpacing : `${ltrSpacing}em`;

    classes[`.text-${name}`] = {
      fontSize: `${size}px`,
      lineHeight: `${lineHeight}px`,
      fontWeight: fontWeight - 100,
      letterSpacing,
    };
  });

  return classes;
}

export const TYPOGRAPHY = generateTypography();

export const TRANSITION = {
  '.base-transition': {
    transitionProperty:
      'background-color, border-color, color, fill, stroke, opacity, box-shadow, transform, border-radius',
    transitionDuration: '0.3s',
    transitionTimingFunction: 'cubic-bezier(0.4, 0, 0.2, 1)',
  },
};

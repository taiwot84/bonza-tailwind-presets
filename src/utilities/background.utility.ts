export const BACKGROUND = {
  '.bg-dots': {
    backgroundImage: 'radial-gradient(#d4dae6 1px, #f4f5fa 1px)',
    backgroundSize: '16px 16px',
  },
};

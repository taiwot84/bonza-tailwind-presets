export const BORDER_RADIUS = {
  none: '0',
  1: '4px',
  '2/3': '6px',
  2: '8px',
  3: '12px',
  4: '16px',
  5: '20px',
  6: '24px',
  full: '9999px',
  inherit: 'inherit',
};

export const SHADOW = {
  none: 'none',
  0: 'inset 0px 1px 2px rgba(62, 79, 102, 0.15), inset 0px 6px 12px -1px rgba(97, 128, 168, 0.12)',
  1: '0px 0px 1px rgba(62, 79, 102, 0.14), 0px 2px 2px rgba(97, 128, 168, 0.25)',
  2: '0px 0px 2px rgba(62, 79, 102, 0.1), 0px 4px 6px rgba(97, 128, 168, 0.17)',
  3: '0px 1px 4px rgba(62, 79, 102, 0.08), 1px 8px 12px rgba(97, 128, 168, 0.15)',
  4: '0px 2px 8px rgba(62, 79, 102, 0.06), 0px 14px 24px rgba(97, 128, 168, 0.14)',
  line: 'inset 0px 0.5px 0px #E8EAF2, inset 0px -0.5px 0px #E8EAF2',
  'line-primary': 'inset 0px -0.5px 0px #0082FF, inset 0px 0.5px 0px #0082FF',
  glow: '0px 2px 20px rgba(0, 130, 255, 0.15)',
};

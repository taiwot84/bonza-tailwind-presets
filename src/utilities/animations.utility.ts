export const KEYFRAMES = {
  shake: {
    '59%': { 'margin-left': 0 },
    '60%, 80%': { 'margin-left': '2px' },
    '70%, 90%': { 'margin-left': '-2px' },
  },

  marquee: {
    '0%': {
      transform: 'translateX(0)',
    },
    '100%': {
      transform: 'translateX(-100%)',
    },
  },

  'loading-dash': {
    '0%': {
      'stroke-dasharray': '1, 200',
      'stroke-dashoffset': '0',
    },
    '50%': {
      'stroke-dasharray': '100, 200',
      'stroke-dashoffset': '-15px',
    },
    '100%': {
      'stroke-dasharray': '100, 200',
      'stroke-dashoffset': '-125px',
    },
  },

  loading: {
    '100%': {
      transform: 'rotate(360deg)',
    },
  },
};

export const ANIMATIONS = {
  'v-shake': 'shake 0.4s cubic-bezier(0.25, 0.8, 0.5, 1)',
  'v-marquee': 'marquee 4.0s linear infinite',
  'v-loading': 'loading 1.4s linear infinite',
  'v-loading-dash': 'loading-dash 1.4s ease-in-out infinite',
};

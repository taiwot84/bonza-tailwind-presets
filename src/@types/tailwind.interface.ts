export type FONT_SIZE_NAMES =
  | 'display1'
  | 'display2'
  | 'h1'
  | 'h2'
  | 'h3'
  | 'h4'
  | 'h5'
  | 'label1'
  | 'label2'
  | 'label3'
  | 'label4'
  | 'label5'
  | 'body-xl'
  | 'body-l'
  | 'body-m'
  | 'body-s'
  | 'body-xs'
  | 'data-label';

export type FONT_DEFINITIONS = [FONT_SIZE_NAMES, number, number, number, number | string];
